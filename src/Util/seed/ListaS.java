/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util.seed;

import java.util.Iterator;
import java.util.Objects;

/**
 * https://pythontutor.com/visualize.html#code=%20class%20ListaS%3CT%3E%7B%0A%20%20%0A%20%20%0A%20%20private%20Nodo%3CT%3E%20cabeza%3B%0A%20%20%20%20private%20int%20size%3B%0A%0A%20%20%20%20public%20ListaS%28%29%20%7B%0A%20%20%20%20%20%20this.cabeza%3Dnull%3B%0A%20%20%20%20%20%20this.size%3D0%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20int%20getSize%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20size%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20void%20insertarInicio%28T%20info%29%0A%20%20%20%20%7B%0A%20%20%20%20%20%20%20%20this.cabeza%3Dnew%20Nodo%28info,%20this.cabeza%29%3B%0A%20%20%20%20%20%20%20%20this.size%2B%2B%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20public%20String%20toString%28%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20%22Lista%20Vac%C3%ADa%22%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20String%20msg%20%3D%20%22%22%3B%0A%20%20%20%20%20%20%20%20for%20%28Nodo%3CT%3E%20i%20%3D%20this.cabeza%3B%20i%20!%3D%20null%3B%20i%20%3D%20i.getSig%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20msg%20%2B%3D%20i.toString%28%29%20%2B%20%22-%3E%22%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20return%20msg%20%2B%20%22null%22%3B%0A%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20boolean%20isVacia%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20this.cabeza%20%3D%3D%20null%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20public%20void%20insertarFin%28T%20info%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20this.insertarInicio%28info%29%3B%0A%20%20%20%20%20%20%20%20%7D%20else%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20Nodo%3CT%3E%20x%3DgetPos%28this.size%20-%201%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20x.setSig%28new%20Nodo%28info,%20null%29%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20this.size%2B%2B%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%0A%20%20%20%20private%20Nodo%3CT%3E%20getPos%28int%20posicion%29%20%7B%0A%20%20%20%20%20%20%20%20this.validar%28posicion%29%3B%0A%20%20%20%20%20%20%20%20Nodo%3CT%3E%20x%20%3D%20this.cabeza%3B%0A%20%20%20%20%20%20%20%20//%20a%3D3,%20b%3Da%3B%20b%3D3%0A%20%20%20%20%20%20%20%20while%20%28posicion--%20%3E%200%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20%20%20%20%20x%20%3D%20x.getSig%28%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20//pos%3Dpos-1%20%3A%20--%3E%20first%20class%20OP%20%3A%28%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20return%20x%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20private%20void%20validar%28int%20i%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%20%7C%7C%20i%20%3C%200%20%7C%7C%20i%20%3E%3D%20this.size%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20throw%20new%20RuntimeException%28%22No%20es%20v%C3%A1lida%20la%20posici%C3%B3n%22%29%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%7D%0A%0A%0A%20class%20Nodo%3CT%3E%7B%0A%20%20%0A%20%20private%20T%20info%3B%0A%20%20private%20Nodo%3CT%3E%20sig%3B%0A%20%20%0A%20%20%0A%20%20Nodo%28%29%20%7B%0A%20%20%20%20%7D%0A%0A%20%20%20%20Nodo%28T%20info,%20Nodo%3CT%3E%20sig%29%20%7B%0A%20%20%20%20%20%20%20%20this.info%20%3D%20info%3B%0A%20%20%20%20%20%20%20%20this.sig%20%3D%20sig%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20T%20getInfo%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20info%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20void%20setInfo%28T%20info%29%20%7B%0A%20%20%20%20%20%20%20%20this.info%20%3D%20info%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20Nodo%3CT%3E%20getSig%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20sig%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20void%20setSig%28Nodo%3CT%3E%20sig%29%20%7B%0A%20%20%20%20%20%20%20%20this.sig%20%3D%20sig%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20public%20String%20toString%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20info.toString%28%29%3B%0A%20%20%20%20%7D%0A%0A%20%20%7D%0A%0A%0A%0A%0A//Paquete%20vista%0Apublic%20class%20TestListaS%20%7B%0A%20%20%20%20public%20static%20void%20main%28String%5B%5D%20args%29%20%7B%0A%20%20%20%20%20%20%0A%20%20%20%20%20%20//Clase%20parametrizada%20%3A%20Integer%0A%20%20%20%20%20%20ListaS%3CInteger%3E%20lista%3Dnew%20ListaS%28%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%284%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%285%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%286%29%3B%0A%20%20%20%20%20%20lista.insertarFin%287%29%3B%0A%20%20%20%20%20%20System.out.println%28lista.toString%28%29%29%3B%0A%0A%20%20%20%20%7D%0A%7D&cumulative=false&curInstr=101&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=java&rawInputLstJSON=%5B%5D&textReferences=false
 *
 * @author Estudiantes
 */
public class ListaS<T> implements Iterable<T>{

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null;
        this.size = 0;
    }

    public int getSize() {
        return size;
    }
    
    public void add(int posicion, T info) {
        this.validar(posicion);
        if (posicion == 0) {
            insertarInicio(info);
        } else {
            Nodo<T> anterior = this.getPos(posicion - 1);//busca el nodo anterior
            anterior.setSig(new Nodo(info, anterior.getSig()));//busca el modo que le sigue al anterior para crear el nuevo nodo
            this.size++;
        }
    }

    public void insertarInicio(T info) {
        this.cabeza = new Nodo(info, this.cabeza);
        this.size++;
    }

    public void insertarFin(T info) {
        if (this.isVacia()) {
            this.insertarInicio(info);
        } else {
            getPos(this.size - 1).setSig(new Nodo(info, null));
            this.size++;
        }
    }

    public void insertarEnPosicion(int index, T info) {
        if (index < 0 || index > getSize()) {
            throw new IndexOutOfBoundsException("indice fuera de rango = " + index);
        } else {
            Nodo<T> nuevo = new Nodo<>();

            Nodo<T> actual = cabeza;
            for (int i = 0; i < index - 1; i++) {
                actual = actual.getSig();
            }
            nuevo = new Nodo<>(info, actual.getSig());
            actual.setSig(nuevo);
        }
    }

    private Nodo<T> getPos(int posicion) {
        this.validar(posicion);
        Nodo<T> x = this.cabeza;
        // a=3, b=a; b=3
        while (posicion-- > 0) {

            x = x.getSig();
            //pos=pos-1 : --> first class OP :(
        }
        return x;
    }

    public void validar(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("No es válida la posición");
        }
    }

    @Override
    public String toString() {
        if (this.isVacia()) {
            return "Lista Vacía";
        }
        String msg = "";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.toString() + "->";
        }
        return msg + "null";

    }

    public boolean isVacia() {
        return this.cabeza == null;
    }

    public T get(int i) {
        try {
            this.validar(i);
            Nodo<T> pos = this.getPos(i);
            return pos.getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public void set(int i, T info) {
        try {
            this.validar(i);
            Nodo<T> pos = this.getPos(i);
            pos.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    public T remove(int i) {
        this.validar(i);
        Nodo<T> borrar = this.cabeza;
        if (i == 0) {
            this.cabeza = this.cabeza.getSig();

        } else {
            Nodo<T> anterior = this.getPos(i - 1);
            borrar = anterior.getSig();
            anterior.setSig(borrar.getSig());
        }

        this.size--;
        borrar.setSig(null);
        return borrar.getInfo();
    }
    
    /**
     * Agrega todos los elementos de la lista especificada al final de esta
     * lista
     *
     * @param l2
     * @return
     */
    public boolean addAll(ListaS<T> l2) {
        if (this.isVacia() && l2.isVacia()) {
            return false;
        }

        if (l2.isVacia()) {
            return false;
        }

        if (this.isVacia()) {
            this.cabeza = l2.cabeza;

        } else {
            Nodo<T> ultimo = this.getPos(size - 1);
            ultimo.setSig(l2.cabeza);
        }
        this.size += l2.size;
        l2.clear();
        return true;
    }

    /**
     * Inserta todos los elementos de la lista especificada en esta lista,
     * comenzando en la posición especificada.
     *
     * @param posicion
     * @param l2
     * @return
     */
    public boolean addAll(int posicion, ListaS<T> l2) {
        if (this.isVacia() && l2.isVacia()) {
            return false;
        }

        if (l2.isVacia()) {
            return false;
        }

        if (this.isVacia()) {
            this.cabeza = l2.cabeza;
        }

        if (posicion == 0) {
            l2.getPos(l2.size - 1).setSig(this.cabeza);
            this.cabeza = l2.cabeza;
        } else {
            Nodo<T> anterior = this.getPos(posicion - 1);
            l2.getPos(l2.size - 1).setSig(anterior.getSig());//como es en cualquier espacio de this lista, entonces
            //calculo el final de l2 y el siguiente de ese va a ser el que esta de siguiente en anterior
            anterior.setSig(l2.cabeza);
        }
        this.size += l2.size;
        l2.clear();
        return true;
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    public boolean isPalin() {
        if (this.isVacia()) {
            throw new RuntimeException("No se puede realizar el proceso");
        }

        ListaS<T> l2 = crearInvertida();
        return (this.equals(l2));

    }

    public ListaS<T> crearInvertida() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        ListaS<T> l2 = new ListaS();
        Nodo<T> ultimo_l2 = null;
        for (Nodo<T> actual = this.cabeza; actual != null; actual = actual.getSig()) {
            T info = actual.getInfo();
            Nodo<T> nuevo_l2 = new Nodo(info, ultimo_l2);
            ultimo_l2 = nuevo_l2;
        }
        l2.cabeza = ultimo_l2;
        l2.size = this.size;
        return l2;

    }

    /**
     * Genera los elementos de la lista en orden inverso
     */
    public void invertir() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        if (this.size == 1) {
            return;
        }
        Nodo<T> ult = null;
        Nodo<T> actual = this.cabeza;
        Nodo<T> sig;
        while (actual != null) {
            sig = actual.getSig();
            actual.setSig(ult);
            ult = actual;
            actual = sig;
        }
        this.cabeza = ult;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.cabeza);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaS<T> other = (ListaS<T>) obj;
        if (this.isVacia() || other.isVacia()) {
            return false;
        }
        if (this.size != other.size) {
            return false;
        }
        // T debe tener implementado equals
        for (Nodo<T> c = this.cabeza, c2 = other.cabeza; c != null; c = c.getSig(), c2 = c2.getSig()) {
            if (!c.getInfo().equals(c2.getInfo())) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return una copia superficial de this.
     */
    public ListaS<T> clone() {
        ListaS<T> listaClonada = new ListaS<>();
        Nodo<T> actual = this.cabeza;
        while (actual != null) {
            listaClonada.insertarFin(actual.getInfo());
            actual = actual.getSig();
        }
        return listaClonada;
    }

    public boolean contains(T info) {
        if (info != null) {
            for (Nodo<T> i = cabeza; i != null; i = i.getSig()) {
                if (info.equals(i.getInfo())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return la información del nodo en la cabeza de la lista
     */
    public T element() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        return this.cabeza.getInfo();
    }

    /**
     * @return la información del ultimo nodo de la lista
     */
    public T getLast() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        Nodo<T> ultimo = getPos(this.size - 1);
        return ultimo.getInfo();
    }

    /**
     * Elimina la cabeza
     *
     * @return retornar el elemento que está en la cabeza de la lista.
     */
    public T poll() {
        if (this.cabeza == null) {
            return null;
        }
        T info = this.cabeza.getInfo();
        this.cabeza = this.cabeza.getSig();
        return info;
    }

    /**
     *
     * @return una matriz que contiene todos los elementos de esta lista
     * elemento).
     */
    public Object[] toArray() {
        Object[] arr = new Object[this.size];
        int i = 0;
        Nodo<T> actual = cabeza;
        while (actual != null) {
            arr[i++] = actual.getInfo();
            actual = actual.getSig();
        }
        return arr;
    }

    public boolean add(T info) {
        if (this.isVacia()) {
            this.insertarInicio(info);
        } else {
            getPos(this.size - 1).setSig(new Nodo(info, null));
            this.size++;
        }
        return true;
    }

    public int getIncidencias(ListaS<T> l2) {
        if (this.isVacia() || l2.isVacia() || l2.getSize() > this.size) {
            throw new RuntimeException("No hay incidencias");
        }
        Nodo<T> cab1, cab2;
        cab1 = this.cabeza;
        cab2 = l2.cabeza;

        int c = 0;
        int incidencia = 0;

        while (cab1 != null) {
            if (cab1.getInfo().equals(cab2.getInfo())) {
                c = 0;
                while (cab1 != null && cab2 != null && cab1.getInfo().equals(cab2.getInfo())) {
                    cab1 = cab1.getSig();
                    cab2 = cab2.getSig();
                    c++;
                }
                if (c == l2.size) {
                    incidencia++;
                }
                cab2 = l2.cabeza;
            } else {
                cab1 = cab1.getSig();
            }
        }
        return incidencia;
    }

    public void insertarOrdenado(T info) {
        if (this.isVacia()) {
            this.insertarInicio(info);
        } else {
            Nodo<T> x = this.cabeza;
            Nodo<T> y = x;
            while (x != null) {
                Comparable comparador = (Comparable) info;
                int rta = comparador.compareTo(x.getInfo());
                if (rta < 0) {
                    break;
                }
                y = x;
                x = x.getSig();
            }
            if (x == y) {
                this.insertarInicio(info);
            } else {
                y.setSig(new Nodo<T>(info, x));
                this.size++;
            }
        }
    }
    
    public int getIndice(T info){        
        int i=0;       
        for(Nodo<T> x=this.cabeza;x!=null;x=x.getSig()){            
            if(x.getInfo().equals(info))
                return (i);            
            i++;            
        }        
        return (-1);        
    } 
    
    @Override
    public Iterator<T> iterator() {
            return new IteratorLS(this.cabeza);
    }
    
}
