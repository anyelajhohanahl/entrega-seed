/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

import java.util.Iterator;

/**
 *
 * @author DOCENTE
 */
public class ListaCD<T extends Comparable> implements Iterable<T> {

    private NodoD<T> cabeza;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public ListaCD() {

        this.cabeza = new NodoD(null, null, null);
        this.cabeza.setAnt(cabeza);
        this.cabeza.setSig(cabeza);
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza.getSig(), this.cabeza);
        this.cabeza.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.size++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.size++;
    }

    @Override
    public String toString() {
        String msg = "";
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig()) {
            msg += x.getInfo() + "<->";
        }
        return "cab<->" + msg + "cab";
    }

    public boolean isVacia() {
        //this.size==0
        //this.cabeza==this.cabeza.getAnt()
        return this.cabeza == this.cabeza.getSig();
    }

    public T remove(int i) {
        validar(i);
        NodoD<T> borrar = this.getPos(i);
        borrar.getAnt().setSig(borrar.getSig());
        borrar.getSig().setAnt(borrar.getAnt());
        this.size--;
        borrar.setAnt(null);
        borrar.setSig(null);
        return borrar.getInfo();
    }

    private void validar(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("No es válida la posición");
        }
    }

    private NodoD<T> getPos(int posicion) {
        this.validar(posicion);
        NodoD<T> x = this.cabeza.getSig();
        // a=3, b=a; b=3
        while (posicion-- > 0) {

            x = x.getSig();
            //pos=pos-1 : --> first class OP :(
        }
        return x;
    }

    public T get(int i) {
        try {
            this.validar(i);
            NodoD<T> pos = this.getPos(i);
            return pos.getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public void set(int i, T info) {
        try {
            this.validar(i);
            NodoD<T> pos = this.getPos(i);
            pos.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorLCD(this.cabeza);
    }

    //interseccion de conjuntos
    public ListaCD<T> getInterseccion(ListaCD<T> l1) {
        if (this.isVacia() || l1.isVacia()) {
            throw new RuntimeException("Al menos una de las listas esta vacia");
        }
        ListaCD<T> result = new ListaCD();
        ListaCD<T> mayor = this;
        ListaCD<T> menor = l1;
        if (this.size < l1.size) {
            mayor = l1;
            menor = this;
        }
        Iterator<T> it = menor.iterator();
        while (it.hasNext()) {
            T info = it.next();
            if (mayor.contains(info)) {
                result.insertarFin(info);
            }
        }
        return result;
    }

    public boolean contains(T info) {
        Iterator<T> it = this.iterator();
        while (it.hasNext()) {
            if (it.next().equals(info)) {
                return true;
            }
        }
        return false;
    }

    /**
     * l1 queda vacío no crear nodos inserta despues de i
     *
     * @param l1
     * @param i
     */
    public void concat(ListaCD<T> l1, int i) {

        if (this.isVacia() && l1.isVacia()) {
            throw new RuntimeException("ambas listas vacias");
        }
        if (this.isVacia()) {
            this.cabeza = l1.cabeza;
        }
        if (l1.isVacia()) {
            this.cabeza = this.cabeza;// o return ?
        } else {
            NodoD<T> ultimo = this.getPos(i);
            ultimo.setSig(l1.cabeza.getSig());
            this.cabeza.setAnt(l1.cabeza.getAnt());
            l1.cabeza.getSig().setAnt(ultimo);
            l1.cabeza.getAnt().setSig(this.cabeza);

            this.size += l1.size;
        }
        l1.clear();
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    /**
     * Método que permite operar con 2 listas ordenadas ascendentemente y
     * retorna la intersección en otra lista ordenada
     *
     * @param l1 Una lista ordenada
     * @return Una lista tipo T
     */
    public ListaCD<T> getInterseccionOrdenado(ListaCD<T> l1) {
        if (this.isVacia() || l1.isVacia()) {
            throw new RuntimeException("Al menos una de las listas esta vacia");
        }
        ListaCD<T> result = new ListaCD();
        ListaCD<T> mayor = this;
        ListaCD<T> menor = l1;
        if (this.size < l1.size) {
            mayor = l1;
            menor = this;
        }
        Iterator<T> it = menor.iterator();
        while (it.hasNext()) {
            T info = it.next();
            if (mayor.contains_sort(info)) {
                result.insertarFin(info);
            }
        }
        return result;
    }

    /**
     * Obtiene verdadero o falso si un elemento existe en una lista ordenada
     * ascendentemente No usar iterador
     *
     * @param elemento
     * @return
     */
    public boolean contains_sort(T elemento) {
        T infoDelinicio = this.cabeza.getSig().getInfo();
        T infoDelfin = this.cabeza.getAnt().getInfo();

        if ((elemento.compareTo(infoDelinicio) < 0) || (elemento.compareTo(infoDelfin) > 0) || this.isVacia()) {
            throw new RuntimeException("elemento inexistente en esta lista");
        }

        int indiceInferior = 0, indiceSuperior = this.size - 1, indiceMedio = (indiceSuperior + indiceInferior) / 2, distanciaHastaMitad = 0;
        NodoD<T> datoMitad = this.getPos(indiceMedio);
        T infoMitad = datoMitad.getInfo();

        while (indiceSuperior >= indiceInferior) {

            if (elemento.compareTo(infoMitad) == 0) {
                return true;
            } else if (elemento.compareTo(infoMitad) < 0) {
                indiceSuperior = indiceMedio - 1;
                int nuevoIndiceM = (indiceSuperior + indiceInferior) / 2;
                distanciaHastaMitad = indiceMedio - nuevoIndiceM;
                indiceMedio = nuevoIndiceM;
                while (distanciaHastaMitad > 0) {
                    datoMitad = datoMitad.getAnt();
                    distanciaHastaMitad--;
                }
                infoMitad = datoMitad.getInfo();
            } else {
                indiceInferior = indiceMedio + 1;
                int nuevoIndiceM = (indiceSuperior + indiceInferior) / 2;
                distanciaHastaMitad = nuevoIndiceM - indiceMedio;
                indiceMedio = nuevoIndiceM;
                while (distanciaHastaMitad > 0) {
                    datoMitad = datoMitad.getSig();
                    distanciaHastaMitad--;
                }
                infoMitad = datoMitad.getInfo();
            }
        }

        return false;
    }
}
