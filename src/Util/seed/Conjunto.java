/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 * Wrapper (Envoltorio)
 *
 * @author docente
 * @param <T>
 */
public class Conjunto<T> implements IConjunto<T> {

    private ListaS<T> elementos;

    public Conjunto() {
        elementos = new ListaS<>();
    }

    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        if (this.elementos.isVacia() || c1.elementos.isVacia()) {
            throw new RuntimeException("alguna lista vacia");
        }
        Conjunto<T> resultante = new Conjunto();
        Conjunto<T> mayor = this;
        Conjunto<T> menor = c1;
        if (this.elementos.getSize() < c1.elementos.getSize()) {
            mayor = c1;
            menor = this;
        }
        for (T elemento : mayor.elementos) {
            if (!resultante.elementos.contains(elemento)) {
                resultante.elementos.insertarOrdenado(elemento);
            }
        }
        for (T element : menor.elementos) {
            if (!resultante.elementos.contains(element)) {
                resultante.elementos.insertarOrdenado(element);
            }
        }
        return resultante;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        if (this.elementos.isVacia() || c1.elementos.isVacia()) {
            throw new RuntimeException("alguna lista vacia");
        }
        Conjunto<T> resultante = new Conjunto();
        Conjunto<T> mayor = this;
        Conjunto<T> menor = c1;
        if (this.elementos.getSize() < c1.elementos.getSize()) {
            mayor = c1;
            menor = this;
        }
        for (T element : menor.elementos) {
            if (mayor.elementos.contains(element) && (!resultante.elementos.contains(element))) {
                resultante.elementos.insertarFin(element);
            }

        }
        return resultante;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        if (this.elementos.isVacia() || c1.elementos.isVacia()) {
            throw new RuntimeException("alguna lista vacia");
        }
        Conjunto<T> resultante = new Conjunto();
        Conjunto<T> mayor = this;
        Conjunto<T> menor = c1;
        if (this.elementos.getSize() < c1.elementos.getSize()) {
            mayor = c1;
            menor = this;
        }
        for (T element : menor.elementos) {
            if (!mayor.elementos.contains(element) && (!resultante.elementos.contains(element))) {
                resultante.elementos.insertarFin(element);
            }
        }
        return resultante;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        if (this.elementos.isVacia() || c1.elementos.isVacia()) {
            throw new RuntimeException("alguna lista vacia");
        }
        Conjunto<T> resultante = new Conjunto();
        Conjunto<T> mayor = this;
        Conjunto<T> menor = c1;
        if (this.elementos.getSize() < c1.elementos.getSize()) {
            mayor = c1;
            menor = this;
        }
        for (T element : menor.elementos) {
            if (!mayor.elementos.contains(element) && (!resultante.elementos.contains(element))) {
                resultante.elementos.insertarFin(element);
            }
        }
        for (T elemento : mayor.elementos) {
            if ((!menor.elementos.contains(elemento) && (!resultante.elementos.contains(elemento)))) {
                resultante.elementos.insertarFin(elemento);
            }
        }
        return resultante;
    }

    @Override
    public Conjunto<Conjunto<T>> getPotencia() {
        if (this.elementos.isVacia()) {
            throw new RuntimeException("lista vacia");
        }

        Conjunto<Conjunto<T>> resultante = new Conjunto<>();
        generarPotencia(this.elementos, resultante);
        for (Conjunto<T> elemento : resultante.elementos) {//este for se hizo con el fin de imprimir en vertical
            System.out.println("Conjuto en orden (otra forma de impresion): "+ elemento);
        }
        return resultante;//de igual manera queda la impresion horizontal
    }

    private void generarPotencia(ListaS<T> elementos, Conjunto<Conjunto<T>> resultante) {
        if (elementos.isVacia()) {
            resultante.insertar(new Conjunto());//si esta vacia la lista retornamos el conjunto vacio
            return;
        }

        T elementoActual = elementos.get(0);
        elementos.remove(0);//tomamos el primer elemento de la lista, lo copiamos y eliminamos para no irlo a repetir

        generarPotencia(elementos, resultante);//ahora si volvemos a generar los subconjuntos sin el primrero

        int totalConjuntos = resultante.elementos.getSize();// damos un tamaño
        for (int i = 0; i < totalConjuntos; i++) {
            Conjunto<T> conjuntoActual = resultante.elementos.get(i);
            Conjunto<T> nuevoConjunto = new Conjunto<>();
            nuevoConjunto.elementos = conjuntoActual.elementos.clone();
            nuevoConjunto.insertar(elementoActual);
            resultante.insertar(nuevoConjunto);
        }
    }

    @Override
    public T get(int i) {
        return elementos.get(i);
    }

    @Override
    public void set(int i, T info) {
        elementos.set(i, info);
    }

    @Override
    public void insertar(T info) {
        this.elementos.insertarFin(info);
    }

    @Override
    public String toString() {
        return "Conjunto{" + "elementos=" + elementos + '}';
    }

}
