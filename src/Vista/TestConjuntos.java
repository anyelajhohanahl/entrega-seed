/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Vista;

import Util.seed.Conjunto;
import java.util.Random;

/**
 *
 * @author Anyela Herrera
 */
public class TestConjuntos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random random = new Random();
        Conjunto<Integer> primero = new Conjunto<>();
        for (int i = 0; i < 4; i++) {
            primero.insertar(random.nextInt(10));
        }

        Conjunto<Integer> segundo = new Conjunto<>();
        for (int i = 0; i < 4; i++) {
            segundo.insertar(random.nextInt(10));
        }
        System.out.println("primer conjunto:" + primero);
        System.out.println("segundo conjunto:" + segundo);
        System.out.println("resultado de interseccion:" + primero.getInterseccion(segundo));
        System.out.println("resultado de uion:" + primero.getUnion(segundo));
        System.out.println("resultado de diferencia:" + primero.getDiferencia(segundo));
        System.out.println("resultado de diferencia Simetrica:" + primero.getDiferenciaAsimetrica(segundo));
        System.out.println("resultado de potencia :" + primero.getPotencia());
    }

}
