/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Pila;
import java.util.Scanner;

/**
 *
 * @author DOCENTE
 */
public class testBalanceoSignos {

    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        /**
         * Ejemplo: L={aa} => --> V L={[aa]} => --> F L=(a)[a]{b}--> V
         */
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();

        Pila<Character> pilaEntrada = new Pila();
        boolean esCorrecta = true;

        for (int i = 0; i < s.length() && esCorrecta; i++) {
            char caracterOrdenamiento = s.charAt(i);

            switch (caracterOrdenamiento) {
                case '(':
                    if (pilaEntrada.isVacia()) {
                        pilaEntrada.push(caracterOrdenamiento);// agrego el nuevo
                    } else {
                        char tmp = pilaEntrada.pop();
                        if (pilaEntrada.size() > 1 && tmp == '[') { // por orden iria [{()}]
                            esCorrecta = false;
                        } else {
                            pilaEntrada.push(tmp);// agrego el que borré
                            pilaEntrada.push(caracterOrdenamiento);// agrego el nuevo
                        }
                    }
                    break;
                case '{':
                    if (pilaEntrada.isVacia()) {
                        pilaEntrada.push(caracterOrdenamiento);// agrego el nuevo
                    } else {
                        char tmp = pilaEntrada.pop();
                        if (pilaEntrada.size() > 1 && tmp == '(') { // por orden iria [{()}]
                            esCorrecta = false;
                        } else {
                            pilaEntrada.push(tmp);// agrego el que borré
                            pilaEntrada.push(caracterOrdenamiento);// agrego el nuevo
                        }
                    }

                    break;

                case '[':
                    if (pilaEntrada.isVacia()) {
                        pilaEntrada.push(caracterOrdenamiento);// agrego el nuevo
                    } else {
                        char tmp = pilaEntrada.pop();
                        if (pilaEntrada.size() > 1 && tmp == '(' || tmp == '{') { // por orden iria [{()}]
                            esCorrecta = false;
                        } else {
                            pilaEntrada.push(tmp);// agrego el que borré
                            pilaEntrada.push(caracterOrdenamiento);// agrego el nuevo
                        }
                    }
                    break;

                case ')':
                    if (pilaEntrada.isVacia() || pilaEntrada.pop() != '(') {
                        esCorrecta = false;
                    }
                    break;

                case '}':
                    if (pilaEntrada.isVacia() || pilaEntrada.pop() != '{') {
                        esCorrecta = false;
                    }
                    break;

                case ']':
                    if (pilaEntrada.isVacia() || pilaEntrada.pop() != '[') {
                        esCorrecta = false;
                    }
                    break;

                default:
                    throw new RuntimeException("Caracter de entrada no válido: " + caracterOrdenamiento);
            }
        }

        if (esCorrecta && pilaEntrada.isVacia()) {
            System.out.println("La cadena es válida");
        } else {
            System.out.println("La cadena es inválida");
        }
    }
}
